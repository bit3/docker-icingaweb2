#!/usr/bin/env bash

function read_file_or_value {
  if [[ -n "$1" ]]; then
    echo $(cat "$1")
  else
    echo "$2"
  fi
}

export HTDOCS_PATH=$(read_file_or_value "${HTDOCS_PATH_FILE}" "${HTDOCS_PATH}")

export WEB_DB_HOST=$(read_file_or_value "${WEB_DB_HOST_FILE}" "${WEB_DB_HOST}")
export WEB_DB_PORT=$(read_file_or_value "${WEB_DB_PORT_FILE}" "${WEB_DB_PORT}")
export WEB_DB_USER=$(read_file_or_value "${WEB_DB_USER_FILE}" "${WEB_DB_USER}")
export WEB_DB_PASS=$(read_file_or_value "${WEB_DB_PASS_FILE}" "${WEB_DB_PASS}")
export WEB_DB=$(read_file_or_value "${WEB_DB_FILE}" "${WEB_DB}")

export IDO_DB_HOST=$(read_file_or_value "${IDO_DB_HOST_FILE}" "${IDO_DB_HOST:=$WEB_DB_HOST}")
export IDO_DB_PORT=$(read_file_or_value "${IDO_DB_PORT_FILE}" "${IDO_DB_PORT:=$WEB_DB_PORT}")
export IDO_DB_USER=$(read_file_or_value "${IDO_DB_USER_FILE}" "${IDO_DB_USER:=$WEB_DB_USER}")
export IDO_DB_PASS=$(read_file_or_value "${IDO_DB_PASS_FILE}" "${IDO_DB_PASS:=$WEB_DB_PASS}")
export IDO_DB=$(read_file_or_value "${IDO_DB_FILE}" "${IDO_DB}")

export DIRECTOR_DB_HOST=$(read_file_or_value "${DIRECTOR_DB_HOST_FILE}" "${DIRECTOR_DB_HOST:=$WEB_DB_HOST}")
export DIRECTOR_DB_PORT=$(read_file_or_value "${DIRECTOR_DB_PORT_FILE}" "${DIRECTOR_DB_PORT:=$WEB_DB_PORT}")
export DIRECTOR_DB_USER=$(read_file_or_value "${DIRECTOR_DB_USER_FILE}" "${DIRECTOR_DB_USER:=$WEB_DB_USER}")
export DIRECTOR_DB_PASS=$(read_file_or_value "${DIRECTOR_DB_PASS_FILE}" "${DIRECTOR_DB_PASS:=$WEB_DB_PASS}")
export DIRECTOR_DB=$(read_file_or_value "${DIRECTOR_DB_FILE}" "${DIRECTOR_DB}")

export ICINGA_API_HOST=$(read_file_or_value "${ICINGA_API_HOST_FILE}" "${ICINGA_API_HOST}")
export ICINGA_API_PORT=$(read_file_or_value "${ICINGA_API_PORT_FILE}" "${ICINGA_API_PORT}")
export ICINGA_API_USERNAME=$(read_file_or_value "${ICINGA_API_USERNAME_FILE}" "${ICINGA_API_USERNAME}")
export ICINGA_API_PASSWORD=$(read_file_or_value "${ICINGA_API_PASSWORD_FILE}" "${ICINGA_API_PASSWORD}")

export MAIL_HUB=$(read_file_or_value "${MAIL_HUB_FILE}" "${MAIL_HUB}")
export MAIL_ROOT=$(read_file_or_value "${MAIL_ROOT_FILE}" "${MAIL_ROOT}")
export MAIL_REWRITE_DOMAIN=$(read_file_or_value "${MAIL_REWRITE_DOMAIN_FILE}" "${MAIL_REWRITE_DOMAIN}")
export MAIL_HOSTNAME=$(read_file_or_value "${MAIL_HOSTNAME_FILE}" "${MAIL_HOSTNAME}")
export MAIL_USERNAME=$(read_file_or_value "${MAIL_USERNAME_FILE}" "${MAIL_USERNAME}")
export MAIL_PASSWORD=$(read_file_or_value "${MAIL_PASSWORD_FILE}" "${MAIL_PASSWORD}")
