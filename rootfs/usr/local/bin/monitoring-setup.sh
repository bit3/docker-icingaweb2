#!/bin/bash
set -e
umask 0002

if [[ ! -e /etc/icingaweb2/enabledModules/monitoring ]]; then
  echo -e "\e[31mMonitoring not enabled, skipping configuration\e[0m"
  exit 0
fi

. /usr/local/bin/setenv.sh

if [[ ! -e /etc/icingaweb2/modules/monitoring/config.ini ]]; then
  echo -e "\e[31mConfigure monitoring\e[0m"
  mkdir -p /etc/icingaweb2/modules/monitoring
  cat <<EOF > /etc/icingaweb2/modules/monitoring/config.ini
[security]
protected_customvars = "*pw*,*pass*,community"
EOF
fi

if [[ ! -e /etc/icingaweb2/modules/monitoring/backends.ini ]]; then
  echo -e "\e[31mConfigure monitoring backend\e[0m"
  mkdir -p /etc/icingaweb2/modules/monitoring
  cat <<EOF > /etc/icingaweb2/modules/monitoring/backends.ini
[icinga]
type = "ido"
resource = "icinga_ido"
EOF
fi

if [[ ! -e /etc/icingaweb2/modules/monitoring/commandtransports.ini ]]; then
  echo -e "\e[31mConfigure monitoring commandtransports\e[0m"
  mkdir -p /etc/icingaweb2/modules/monitoring
  cat <<EOF > /etc/icingaweb2/modules/monitoring/commandtransports.ini
[icinga2]
transport = "api"
host = "$ICINGA_API_HOST"
port = "$ICINGA_API_PORT"
username = "$ICINGA_API_USERNAME"
password = "$ICINGA_API_PASSWORD"
EOF
fi
