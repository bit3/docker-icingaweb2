#!/usr/bin/env bash
set -e
umask 0002

. /usr/local/bin/setenv.sh

echo -e "\e[31mInstalling icinga web htdocs\e[0m"

mkdir -p "$HTDOCS_PATH"

rsync -a --delete --exclude '/index.php' /opt/icingaweb2/public/ "$HTDOCS_PATH/"

cat <<EOF > "$HTDOCS_PATH/index.php"
<?php
require_once '/opt/icingaweb2/public/index.php';
EOF
