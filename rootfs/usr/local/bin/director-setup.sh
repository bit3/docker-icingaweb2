#!/bin/bash
set -e
umask 0002

if [[ ! -e /etc/icingaweb2/enabledModules/director ]]; then
  echo -e "\e[31mDirector not enabled, skipping configuration\e[0m"
  exit 0
fi

. /usr/local/bin/setenv.sh

function director_db_psql {
  PGPASSWORD="$DIRECTOR_DB_PASS" psql -h "$DIRECTOR_DB_HOST" -p "$DIRECTOR_DB_PORT" -U "$DIRECTOR_DB_USER" "$DIRECTOR_DB" "$@"
  return $?
}

function director_db_check {
  db-check.py -h "$DIRECTOR_DB_HOST" -P "$DIRECTOR_DB_PORT" -u "$DIRECTOR_DB_USER" -p "$DIRECTOR_DB_PASS" -d "$DIRECTOR_DB" "$@"
}

if [[ ! -e /etc/icingaweb2/modules/director/config.ini ]]; then
  echo -e "\e[31mConfigure director\e[0m"
  mkdir -p /etc/icingaweb2/modules/director
  cat <<EOF > /etc/icingaweb2/modules/director/config.ini
[db]
resource = "director_db"
EOF
fi

director_db_check "await-up"

director_db_check "database-exists" || (
  echo -e "\e[31mDatabase $DIRECTOR_DB does not exists, aborting...\e[0m"
  exit 1
)

director_db_check "table-exists" "director_activity_log" || (
  echo -e "\e[31mInitial import director database\e[0m"
  director_db_psql < /opt/icingaweb2/modules/director/schema/pgsql.sql
)

icingacli director migration pending && (
  echo -e "\e[31mRun director migration\e[0m"
  icingacli director migration run
)

if [[ -e /etc/icingaweb2/modules/director/kickstart.ini ]]; then
  echo -e "\e[31mRun director kickstart\e[0m"
  icingacli director kickstart run
fi
