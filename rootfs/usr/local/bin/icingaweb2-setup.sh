#!/bin/bash
set -e
umask 0002

. /usr/local/bin/setenv.sh

# Set timezone for PHP
echo "date.timezone = ${TIMEZONE:-UTC}" > /usr/local/etc/php/conf.d/zz-timezone.ini

# Set up config directory
icingacli setup config directory

# Generate or show setup token
echo -e "\e[31m========================================\e[0m"
echo
if [[ ! -f /etc/icingaweb2/setup.token ]]; then
  icingacli setup token create
else
  icingacli setup token show
fi
echo
echo -e "\e[31m========================================\e[0m"
echo

# Enable modules
if [[ ! -e /etc/icingaweb2/enabledModules || $(ls -1 /etc/icingaweb2/enabledModules | wc -l) -le 0 ]]; then
  echo -e "\e[31mEnable module monitoring\e[0m"
  icingacli module enable monitoring
  echo -e "\e[31mEnable module doc\e[0m"
  icingacli module enable doc
  echo -e "\e[31mEnable module translation\e[0m"
  icingacli module enable translation
  echo -e "\e[31mEnable module director\e[0m"
  icingacli module enable director
  echo -e "\e[31mEnable module cube\e[0m"
  icingacli module enable cube
  echo -e "\e[31mEnable module businessprocess\e[0m"
  icingacli module enable businessprocess
fi

# Setup config.ini
if [[ ! -e /etc/icingaweb2/config.ini ]]; then
  echo -e "\e[31mConfigure basics\e[0m"
  mkdir -p /usr/local/icingaweb2/modules
  cat <<EOF > /etc/icingaweb2/config.ini
[global]
show_stacktraces = "1"
show_application_state_messages = "1"
module_path = "/usr/local/icingaweb2/modules:/opt/icingaweb2/modules"
config_backend = "db"
config_resource = "icingaweb_db"

[logging]
log = "syslog"
level = "ERROR"
application = "icingaweb2"
facility = "user"
EOF
fi

# Setup authentication.ini
if [[ ! -e /etc/icingaweb2/authentication.ini ]]; then
  echo -e "\e[31mConfigure authentication\e[0m"
  cat <<EOF > /etc/icingaweb2/authentication.ini
[icingaweb2]
backend = "db"
resource = "icingaweb_db"
EOF
fi

# Setup groups.ini
if [[ ! -e /etc/icingaweb2/groups.ini ]]; then
  echo -e "\e[31mConfigure groups\e[0m"
  cat <<EOF > /etc/icingaweb2/groups.ini
[icingaweb2]
backend = "db"
resource = "icingaweb_db"
EOF
fi

# Setup roles.ini
if [[ ! -e /etc/icingaweb2/roles.ini ]]; then
  echo -e "\e[31mConfigure roles\e[0m"
  cat <<EOF > /etc/icingaweb2/roles.ini
[Administrators]
users = "admin"
permissions = "*"
groups = "Administrators"
EOF
fi

# Setup resources.ini
if [[ ! -e /etc/icingaweb2/resources.ini ]]; then
  echo -e "\e[31mConfigure resources\e[0m"
  cat <<EOF > /etc/icingaweb2/resources.ini
[icingaweb_db]
type = "db"
db = "pgsql"
host = "${WEB_DB_HOST}"
port = "${WEB_DB_PORT}"
dbname = "${WEB_DB}"
username = "${WEB_DB_USER}"
password = "${WEB_DB_PASS}"
charset = "utf8"

[icinga_ido]
type = "db"
db = "pgsql"
host = "${IDO_DB_HOST}"
port = "${IDO_DB_PORT}"
dbname = "${IDO_DB}"
username = "${IDO_DB_USER}"
password = "${IDO_DB_PASS}"
charset = "utf8"

[director_db]
type = "db"
db = "pgsql"
host = "${DIRECTOR_DB_HOST}"
port = "${DIRECTOR_DB_PORT}"
dbname = "${DIRECTOR_DB}"
username = "${DIRECTOR_DB_USER}"
password = "${DIRECTOR_DB_PASS}"
charset = "utf8"

;[ssh]
;type = "ssh"
;user = "${CMDPIPE_USER}"
;private_key = "/tmp/key_for_commandtransport"
EOF
fi
