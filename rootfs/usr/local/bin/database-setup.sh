#!/usr/bin/env bash
set -ex
umask 0002

. /usr/local/bin/setenv.sh

function web_db_psql {
  PGPASSWORD="$WEB_DB_PASS" psql -h "$WEB_DB_HOST" -p "$WEB_DB_PORT" -U "$WEB_DB_USER" "$WEB_DB" "$@"
  return $?
}

function web_db_check {
  db-check.py -h "$WEB_DB_HOST" -P "$WEB_DB_PORT" -u "$WEB_DB_USER" -p "$WEB_DB_PASS" -d "$WEB_DB" "$@"
}

web_db_check "await-up"

web_db_check "database-exists" || (
  echo -e "\e[31mDatabase $WEB_DB does not exists, aborting...\e[0m"
  exit 1
)

web_db_check "table-exists" "icingaweb_user" || (
  echo -e "\e[31mInitial import database\e[0m"
  web_db_psql < /opt/icingaweb2/etc/schema/pgsql.schema.sql

  # create admin user. Pass: admin
  ADMIN_PASS='$1$7akZL1zu$3u0pors2zz.6PnQViccld1'
  web_db_psql -c "INSERT INTO icingaweb_user (name, active, password_hash) VALUES ('admin', 1, '$ADMIN_PASS');"
  echo -e "\e[31m!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\e[0m"
  echo -e "\e[31m!!! User admin with pass admin created. PLEASE CHANGE !!!\e[0m"
  echo -e "\e[31m!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\e[0m"
  echo
)

# TODO Database upgrade path
