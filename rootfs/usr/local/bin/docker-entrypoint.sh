#!/usr/bin/env bash
set -e

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
  set -- php-fpm "$@"
fi

if [ "$1" = "php-fpm" ]; then

  if [[ -x /usr/local/bin/database-setup.sh ]]; then
    /usr/local/bin/database-setup.sh
  fi

  if [[ -x /usr/local/bin/icingaweb2-setup.sh ]]; then
    /usr/local/bin/icingaweb2-setup.sh
  fi

  if [[ -x /usr/local/bin/director-setup.sh ]]; then
    /usr/local/bin/director-setup.sh
  fi

  if [[ -x /usr/local/bin/monitoring-setup.sh ]]; then
    /usr/local/bin/monitoring-setup.sh
  fi

  if [[ -x /usr/local/bin/ssmtp-setup.sh ]]; then
    /usr/local/bin/ssmtp-setup.sh
  fi

  if [[ -x /usr/local/bin/htdocs-setup.sh ]]; then
    /usr/local/bin/htdocs-setup.sh
  fi

fi

exec "$@"
