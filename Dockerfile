FROM php:7.2-fpm

ENV REFRESHED_AT="2018-09-07"\
    ICINGAWEB_VERSION="2.6.1" \
    DIRECTOR_VERSION="1.5.1" \
    CUBE_VERSION="1.0.1" \
    GRAFANA_VERSION="1.3.1" \
    BUSINESSPROCESS_VERSION="2.1.0" \
    HTDOCS_PATH="/var/www/html"\
    TIMEZONE="UTC" \
    ICINGA_API_HOST="icinga" \
    ICINGA_API_PORT="5665" \
    ICINGA_API_USERNAME="root" \
    ICINGA_API_PASSWORD="super-secret" \
    WEB_DB_HOST="postgres" \
    WEB_DB_PORT="5432" \
    WEB_DB_USER="postgres" \
    WEB_DB_PASS="postgres" \
    WEB_DB="icinga_web" \
    IDO_DB_HOST="" \
    IDO_DB_PORT="" \
    IDO_DB_USER="" \
    IDO_DB_PASS="" \
    IDO_DB="icinga" \
    DIRECTOR_DB_HOST="" \
    DIRECTOR_DB_PORT="" \
    DIRECTOR_DB_USER="" \
    DIRECTOR_DB_PASS="" \
    DIRECTOR_DB="icinga_director"

RUN set -xe; \
    export DEBIAN_FRONTEND=noninteractive; \
    mkdir mkdir -p /usr/share/man/man1 /usr/share/man/man7; \
    apt update; \
    apt install -y \
      # build packages, can be removed after build
      libcurl4-openssl-dev \
      libicu-dev \
      libxml2-dev \
      libpq-dev \
      libpng-dev \
      wget \
      # runtime packages
      postgresql-client \
      libpq5 \
      libpng16-16 \
      rsync \
      python3-psycopg2 \
      libfcgi0ldbl \
      mailutils \
      ssmtp \
      rsyslog; \
    # build php modules
    docker-php-ext-install -j$(nproc) ctype; \
    docker-php-ext-install -j$(nproc) curl; \
    docker-php-ext-install -j$(nproc) gettext; \
    docker-php-ext-install -j$(nproc) iconv; \
    docker-php-ext-install -j$(nproc) intl; \
    docker-php-ext-install -j$(nproc) json; \
    docker-php-ext-install -j$(nproc) mbstring; \
    docker-php-ext-install -j$(nproc) pgsql; \
    docker-php-ext-install -j$(nproc) pdo_pgsql; \
    docker-php-ext-install -j$(nproc) xml; \
    # Download sources
    echo "Fetch Icingaweb2 ${ICINGAWEB_VERSION}"; \
    mkdir /opt/icingaweb2; \
    wget -q -O - https://github.com/Icinga/icingaweb2/archive/v${ICINGAWEB_VERSION}.tar.gz \
      | tar xz --strip 1 -C /opt/icingaweb2; \
    echo "Fetch Module Director ${DIRECTOR_VERSION}"; \
    mkdir -p /opt/icingaweb2/modules/director; \
    wget -q -O - https://github.com/Icinga/icingaweb2-module-director/archive/v${DIRECTOR_VERSION}.tar.gz \
      | tar xz --strip 1 -C /opt/icingaweb2/modules/director; \
    ln -s /opt/icingaweb2/bin/icingacli /usr/bin/icingacli; \
    echo "Fetch Module Cube ${CUBE_VERSION}"; \
    mkdir -p /opt/icingaweb2/modules/cube; \
    wget -q -O - https://github.com/Icinga/icingaweb2-module-cube/archive/v${CUBE_VERSION}.tar.gz \
      | tar xz --strip 1 -C /opt/icingaweb2/modules/cube --strip 1; \
    echo "Fetch Module Businessprocess ${BUSINESSPROCESS_VERSION}"; \
    mkdir -p /opt/icingaweb2/modules/businessprocess; \
    wget -q -O - https://github.com/Icinga/icingaweb2-module-businessprocess/archive/v${BUSINESSPROCESS_VERSION}.tar.gz \
      | tar xz --strip 1 -C /opt/icingaweb2/modules/businessprocess --strip 1; \
    echo "Fetch Module Grafana ${GRAFANA_VERSION}"; \
    mkdir -p /opt/icingaweb2/modules/grafana; \
    wget -q -O - https://github.com/Mikesch-mp/icingaweb2-module-grafana/archive/v${GRAFANA_VERSION}.tar.gz \
      | tar xz --strip 1 -C /opt/icingaweb2/modules/grafana --strip 1; \
    # configure php
    echo '[www]' > /usr/local/etc/php-fpm.d/icinga.conf; \
    echo 'ping.path = /ping' >> /usr/local/etc/php-fpm.d/icinga.conf; \
    echo 'chdir = /opt/icingaweb2' >> /usr/local/etc/php-fpm.d/icinga.conf; \
    echo 'php_value[include_path] = .:/opt/icingaweb2/library:/opt/icingaweb2/library/vendor:/usr/local/lib/php' >> /usr/local/etc/php-fpm.d/icinga.conf; \
    echo 'include_path = .:/opt/icingaweb2/library:/opt/icingaweb2/library/vendor:/usr/local/lib/php' > /usr/local/etc/php/conf.d/include_path.ini; \
    # Configure environment
    addgroup --system icingaweb2; \
    usermod -a -G icingaweb2 www-data; \
    icingacli setup config directory; \
    # remove build packages
    apt remove --purge -y \
      libcurl4-openssl-dev \
      libicu-dev \
      libxml2-dev \
      libpq-dev \
      libpng-dev \
      wget; \
    apt autoremove --purge -y; \
    rm -rf /var/lib/apt/lists/*;

ADD rootfs /

VOLUME ["/etc/icingaweb2"]

HEALTHCHECK CMD SCRIPT_NAME=/ping \
                SCRIPT_FILENAME=/ping \
                REQUEST_METHOD=GET \
                cgi-fcgi -bind -connect 127.0.0.1:9000

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["php-fpm"]
